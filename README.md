# Projekt końcowy #

1. Celem projektu jest wytworzenie działającej aplikacji od zera. Nie chodzi o wykorzystanie konkretnej technologii, ani napisanie kawałka kodu. Należy nastawić się na dostarczenie aplikacji, która posiada jakąś użyteczność z punktu widzenia użytkownika.
1. Projekty wykonujemy samodzielnie lub w grupach. Gorąco polecam grupy dwuosobowe, ale nie chcę narzucać rozmiaru zespołu. Dopuszczalne jest zarówno samodzielne pisanie aplikacji, jak i w grupie 3-4 osobowej.
1. Projekt możemy wykonać w dowolnej technologii, w zależności od indywidualnych upodobań. Aby było wam łatwiej wypisałem kilka przykładów architektury aplikacji.
1. Tematyka projektów jest dowolna. Należy się jednak dobrze zastanowić jaką funkcjonalność będziecie chcieli zrealizować.
1. Najważniejszym założeniem projektu końcowego jest wasza samodzielna praca. Dlatego proponuję zrealizować projekt na miarę możliwości. Każda działająca aplikacja będzie dobrze zrealizowanym projektem.
1. Projekty prowadzimy na gicie. Dokumentacje projektu prowadzimy w formie pliku readme.md.
1. Elementy na które warto zwrócić uwagę (nie są to elementy obowiązkowe):
    * praca grupowa
    * modularność, podział na klasy
    * testy jednostkowe
    * wykorzystanie gita i tworzenie dużej ilości małych commitów
    * wykorzystanie poznanych technologii (hibernate, spring)
    * wykorzystanie nowych technologii
    * oddzielenie logiki od wyglądu aplikacji
    * czytelność kodu
## Przykładowa architektura aplikacji:
1. Aplikacja konsolowa
1. Aplikacja desktopowa z wykorzystaniem Swing/JavaFX
1. Aplikacja desktopowa z wykorzystaniem Servletów i JSP
1. Aplikacja springowa z wykorzystaniem JSP lub Thymeleaf
1. Aplikacja springowa z wydzieloną częścią back-endową i frond-endową (zaimplementowana opcjonalnie)

